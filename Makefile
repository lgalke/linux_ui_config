all: install


install:
	# Installing Plasma Themes...
	cp -ir kde_plasma_themes/Klorax.Dracula ~/.local/share/plasma/desktoptheme/Klorax.Dracula
	cp -ir kde_plasma_themes/Klorax.DraculaOpaque/ ~/.local/share/plasma/desktoptheme/Klorax.DraculaOpaque
	cp -ir kde_plasma_themes/Klorax.Retrofuturism/ ~/.local/share/plasma/desktoptheme/Klorax.Retrofuturism
	# Installing Plasma Color Schemes...
	cp -i kde_plasma_color-schemes/KloraxDracula.colors ~/.local/share/color-schemes/KloraxDracula.colors
	cp -i kde_plasma_color-schemes/KloraxRetrofuturism.colors ~/.local/share/color-schemes/KloraxRetrofuturism.colors
	# Installing Konsole Color Schemes...
	cp -i kde_konsole_color-schemes/Klorax.Dracula.colorscheme ~/.local/share/konsole
	cp -i kde_konsole_color-schemes/Klorax.Retrofuturism.colorscheme ~/.local/share/konsole

